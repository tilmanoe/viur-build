set -e
if [ -d "/workspace/sources" ]; then
	cd /workspace/sources && npm install && gulp
	cd ..
	exit 0
else
 echo "no sources folder"
fi

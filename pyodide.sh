set -e
if [ -d "/workspace/appengine" ]; then
  applicationfolder="/workspace/appengine"
else
  applicationfolder="/workspace/deploy"
fi

pyodidepath="/viur/vi"

if [ -d "$applicationfolder$pyodidepath" ]; then
	cd $applicationfolder$pyodidepath && python3 ./get-pyodide.py
	cd /
	exit 0
else
 echo "no Pyodide Vi folder"
 exit 0
fi

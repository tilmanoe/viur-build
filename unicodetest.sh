set -e

if [ -d "/workspace/appengine" ]; then
  applicationfolder="/workspace/appengine"
else
  applicationfolder="/workspace/deploy"
fi

python /unicodetester.py $applicationfolder

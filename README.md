# viur-build

Docker build environment for ViUR 2.X Projects.

# Build container locally
docker build -t viur_build .



#build
#js, css, icons
RUN ls
RUN cd sources && npm install && gulp
RUN cd ..
#vi
RUN cd vi && make deploy
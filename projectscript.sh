set -e

if [ -f "project.py" ]; then
	python3 project.py
	exit 0
else
 echo "no project.py"
 exit 0
fi

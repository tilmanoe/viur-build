set -e
if [ -f "requirements.txt" ]; then
  if [ -d "/workspace/appengine" ]; then
    pip2 install -r requirements.txt -t appengine/lib
  fi

  if [ -d "/workspace/deploy" ]; then
    pip2 install -r requirements.txt -t deploy/lib
  fi

  exit 0
else
 echo "no requirements.txt found"
fi

set -e
if [ -d "/workspace/vi" ]; then
	cd /workspace/vi && make deploy
	cd ..
	exit 0
else
 echo "no vi folder"
 exit 1
fi

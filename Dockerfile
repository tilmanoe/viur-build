FROM ubuntu:18.04
MAINTAINER Andreas H. Kelch <ak@mausbrand.de>

#install
RUN apt-get update && apt-get install -y \
	autoconf \
	automake \
	git \
	wget \
	python \
	python-pip \
	python2.7-dev \
	python3 \
	python3-pip \
	libjpeg62 \
	libjpeg62-dev \
	zlib1g-dev\
	npm \
	unzip

#update node and npm
RUN apt-get update
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_12.x  | bash -
RUN apt-get -y install nodejs

RUN npm install -g less
RUN npm install -g gulp
RUN pip install git+https://github.com/pyjs/pyjs.git#egg=pyjs
RUN pip3 install requests

# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LANGUAGE=C.UTF-8

# Xvfb provide an in-memory X-session for tests that require a GUI
ENV DISPLAY=:99

COPY buildvi.sh /
COPY gulp.sh /
COPY pip-requirements.sh /
COPY unicodetest.sh /
COPY unicodetester.py /
COPY pyodide.sh /
COPY pyodide3.sh /
COPY projectscript.sh /

# legacy PyJs Vi
RUN ["chmod", "+x", "buildvi.sh"]
# runs gulp task in sources folder
RUN ["chmod", "+x", "gulp.sh"]
# installs pip requirements from /requirements.txt
RUN ["chmod", "+x", "pip-requirements.sh"]
# ensure no unicode files in deploy or appengine folder
RUN ["chmod", "+x", "unicodetest.sh"]
# downloads pyodide libs
RUN ["chmod", "+x", "pyodide.sh"]
RUN ["chmod", "+x", "pyodide3.sh"]
RUN ["chmod", "+x", "projectscript.sh"]

ENTRYPOINT /bin/bash

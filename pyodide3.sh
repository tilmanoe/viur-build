set -e

if [ -f "get-pyodide.py" ]; then
	python3 get-pyodide.py
	exit 0
else
 echo "no get-pyodide.py"
 exit 0
fi
